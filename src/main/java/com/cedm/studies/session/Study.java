package com.cedm.studies.session;

import com.cedm.studies.session.model.Card;
import com.cedm.studies.session.model.StudyProgram;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Scanner;

@Component
public class Study {

    @Resource(name = "Scanner")
    Scanner inputScanner;

    public void letsStudy(StudyProgram studyProgram) {
        while (studyProgram.stillWorkForToday()) {
            Card card = studyProgram.getDailyWork();
            printQuestion(card);
            addCardToStudyProgramm(studyProgram, card);
        }
        if (studyProgram.readyForExam()) {
            System.out.println("Congratulation, studies finish ! ");
        } else {
            System.out.println("Good job for today, see you tomorrow");
            studyProgram.dumpTomorrowProgram();
        }
    }

    private void addCardToStudyProgramm(StudyProgram studyProgram, Card actualStudy) {
        boolean cardProcessed = false;
        while (!cardProcessed) {
            cardProcessed = studyProgram.putInStudyProgram(inputScanner.nextLine(), actualStudy);
            if (!cardProcessed) {
                System.out.println("Error! Please choose between r, o or g");
            } else break;
        }
    }

    private void printQuestion(Card actualStudy) {
        System.out.println(actualStudy.getQuestion());
        System.out.println("Where do you want to put this card ?");
        System.out.println("red (r), orange (o), green (g)");
    }
}
