package com.cedm.studies.session.conf;

import com.cedm.studies.session.StudiesSessionMain;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.Scanner;

@Configuration("serviceConfig")
@ComponentScan(basePackageClasses = StudiesSessionMain.class)
public class Config {

    @Bean(name = "Scanner")
    public Scanner transferService() {
        return new Scanner(System.in);
    }
}
