package com.cedm.studies.session;

import com.cedm.studies.session.mapper.CardsMapper;
import com.cedm.studies.session.model.Card;
import com.cedm.studies.session.model.StudyProgram;
import com.cedm.studies.session.util.FileParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Scanner;

@Component
public class InitSession {

    @Resource(name = "Scanner")
    private Scanner inputScanner;

    @Autowired
    private CardsMapper mapper;

    @Autowired
    private FileParser fileParser;

    public StudyProgram initialize(String filePath) {
        List<String> lines = fileParser.parse(filePath);
        List<Card> cards = mapper.map(lines);
        StudyProgram studyProgram = new StudyProgram();
        System.out.println("Hello, welcome !");
        System.out.println("Let's letsStudy studies, press enter to continue");
        inputScanner.nextLine();
        addCardsToStudyProgram(cards, studyProgram);
        return studyProgram;
    }

    private void addCardsToStudyProgram(List<Card> cards, StudyProgram studyProgram) {
        int i = 0;
        while (i < cards.size()) {
            Card card = cards.get(i);
            printQuestion(card);
            if(studyProgram.putInStudyProgram(inputScanner.nextLine(), card)) {
                i++;
            } else {
                System.out.println("Error! Please choose between r, o or g");
            }
        }
    }

    private void printQuestion(Card card) {
        System.out.println(card.getQuestion());
        System.out.println("Where do you want to put this card ?");
        System.out.println("red (r), orange (o), green (g)");
    }
}
