package com.cedm.studies.session;

import com.cedm.studies.session.conf.Config;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

import java.io.IOException;


public class AppInitialization {

    public static void main(String[] args) throws IOException, InterruptedException {
        ApplicationContext applicationContext = SpringApplication.run(Config.class);
        StudiesSessionMain main = applicationContext.getBean(StudiesSessionMain.class);
        try {
            main.start(args);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
        System.exit(0);
    }
}
