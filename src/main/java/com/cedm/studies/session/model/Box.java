package com.cedm.studies.session.model;

import java.util.LinkedList;

public class Box {

    private LinkedList<Card> cards = new LinkedList<>();

    public Card takeCard() {
        return cards.pop();
    }

    public void putCard(final Card card) {
        cards.add(card);
    }

    public boolean containCard() {
        return !cards.isEmpty();
    }

    public LinkedList<Card> getAllCards() {
        return cards;
    }
}
