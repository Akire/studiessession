package com.cedm.studies.session.model;

import java.util.Objects;

public class Card {

    private final String question;
    private final String answer;

    public Card(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

    @Override
    public String toString() {
        return question + "|" + answer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return Objects.equals(question, card.question) &&
                Objects.equals(answer, card.answer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(question, answer);
    }
}
