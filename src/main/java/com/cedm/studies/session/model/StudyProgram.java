package com.cedm.studies.session.model;

import com.cedm.studies.session.exception.FileParseException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class StudyProgram {
    public static final Path PATH = Paths.get("studyState.stud");
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    private final Box greenBox = new Box();
    private final Box orangeBox = new Box();
    private final Box redBox = new Box();

    public boolean putInStudyProgram(String boxColor, Card card) {
        switch (boxColor) {
            case "r":
                redBox.putCard(card);
                return true;
            case "o":
                orangeBox.putCard(card);
                return true;
            case "g":
                greenBox.putCard(card);
                return true;
            default:
                return false;
        }
    }

    public boolean readyForExam() {
        return !stillWorkForToday()
                && !orangeBox.containCard();
    }

    public boolean stillWorkForToday() {
        return redBox.containCard();
    }

    public Card getDailyWork() {
        return redBox.takeCard();
    }

    public void dumpTomorrowProgram()  {
        try {
            if (Files.exists(PATH)) {
                Files.delete(PATH);
            }
            Files.createFile(PATH);
            String boxs = boxToString(greenBox, "o") + boxToString(orangeBox, "r");
            Files.write(PATH, boxs.getBytes());
        } catch (Exception e) {
            throw new FileParseException("an exception occured during file parse", e);
        }
    }

    private String boxToString(Box box, String color) {
        return box.getAllCards()
                .stream()
                .map(s -> color + "|" + s.toString() + LINE_SEPARATOR)
                .collect(Collectors.joining());
    }
}
