package com.cedm.studies.session;

import com.cedm.studies.session.model.StudyProgram;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StudiesSessionMain {

    @Autowired
    private InitSession initSession;

    @Autowired
    private InProgressSession inProgressSession;

    @Autowired
    private Study study;

    public void start(String[] args) {
        final StudyProgram studyProgram;
        if(args.length == 1) {
            studyProgram = initSession.initialize(args[0]);
        } else {
            studyProgram = inProgressSession.retrieve();
        }
        study.letsStudy(studyProgram);

    }
}
