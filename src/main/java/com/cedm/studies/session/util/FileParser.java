package com.cedm.studies.session.util;

import com.cedm.studies.session.exception.FileParseException;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class FileParser {

    public List<String> parse(String path) {
        try (Stream<String> stream = Files.lines(Paths.get(path))) {
            return toLines(stream);
        } catch (Exception e) {
            throw new FileParseException(e.getMessage(), e);
        }
    }

    private List<String> toLines(Stream<String> stream) {
        return stream
                .filter(isNotBlankOrEmpty())
                .map(String::trim)
                .collect(Collectors.toList());
    }

    private Predicate<String> isNotBlankOrEmpty() {
        return s -> !s.trim().isEmpty();
    }

}
