package com.cedm.studies.session.mapper;

import com.cedm.studies.session.model.Card;
import com.cedm.studies.session.model.StudyProgram;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class StudyProgramMapper {

    private static final int COLOR = 0;
    private static final int QUESTION = 1;
    private static final int ANSWER = 2;
    public static final String PIPELINE_SPLIT_REGEX = "\\|";

    public StudyProgram map(List<String> lines) {
        List<Pair<String, Card>> pairs = lines
                .stream()
                .map(lineToPair())
                .collect(Collectors.toList());
        return createStudyProgram(pairs);
    }

    private StudyProgram createStudyProgram(List<Pair<String, Card>> cards) {
        StudyProgram studyProgram = new StudyProgram();
        cards.stream()
                .forEach(s -> studyProgram.putInStudyProgram(s.getKey(), s.getValue()));
        return studyProgram;
    }

    private Function<String, Pair<String, Card>> lineToPair() {
        return line -> {
            String[] splitLine = line.split(PIPELINE_SPLIT_REGEX);
            return new ImmutablePair<>(splitLine[COLOR],
                    new Card(splitLine[QUESTION], splitLine[ANSWER]));
        };
    }
}
