package com.cedm.studies.session.mapper;

import com.cedm.studies.session.model.Card;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CardsMapper {


    private static final int QUESTION = 0;
    private static final int ANSWER = 1;
    public static final String PIPELINE_SPLIT_REGEX = "\\|";

    public List<Card> map(final  List<String> lines) {
        List<Card> cards = lines.stream().map(l -> toCard(l)).collect(Collectors.toList());
        cards.sort(alphabeticSort());
        return cards;
    }

    private Comparator<Card> alphabeticSort() {
        return (s1, s2) -> s1.getQuestion().compareTo(s2.getQuestion());
    }

    private Card toCard(final  String line) {
        final String[] splitLine = line.split(PIPELINE_SPLIT_REGEX);
        if (splitLine.length != 2) {
            throw new IllegalArgumentException("File format should be : <question> | <answer>");
        }
        return new Card(splitLine[QUESTION].trim(), splitLine[ANSWER].trim());
    }
}
