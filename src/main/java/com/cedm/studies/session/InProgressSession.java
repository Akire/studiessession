package com.cedm.studies.session;

import com.cedm.studies.session.mapper.StudyProgramMapper;
import com.cedm.studies.session.model.StudyProgram;
import com.cedm.studies.session.util.FileParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InProgressSession {

    private static final String STUDY_STATE_STUD = "studyState.stud";
    @Autowired
    private FileParser fileParser;

    @Autowired
    private StudyProgramMapper mapper;

    public StudyProgram retrieve() {
        List<String> lines = fileParser.parse(STUDY_STATE_STUD);
        return mapper.map(lines);
    }
}
