package com.cedm.studies.session;

import com.cedm.studies.session.model.StudyProgram;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StudiesSessionMainTest {


    @InjectMocks
    StudiesSessionMain tested;

    @Mock
    InitSession initSession;

    @Mock
    InProgressSession inProgressSession;

    @Mock
    Study study;

    @Test
    public void testInitSession() throws Exception {
        StudyProgram studyProgram = mock(StudyProgram.class);
        when(initSession.initialize("test")).thenReturn(studyProgram);

        tested.start(new String[]{"test"});

        verifyZeroInteractions(inProgressSession);
        verify(study).letsStudy(studyProgram);
    }

    @Test
    public void testInProgressSessionWithoutArgument() throws Exception {
        StudyProgram studyProgram = mock(StudyProgram.class);
        when(inProgressSession.retrieve()).thenReturn(studyProgram);

        tested.start(new String[]{});

        verifyZeroInteractions(initSession);
        verify(study).letsStudy(studyProgram);
    }
}