package com.cedm.studies.session;

import com.cedm.studies.session.mapper.StudyProgramMapper;
import com.cedm.studies.session.model.StudyProgram;
import com.cedm.studies.session.util.FileParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InProgressSessionTest {

    @InjectMocks
    InProgressSession tested;

    @Mock
    private FileParser fileParser;

    @Mock
    private StudyProgramMapper mapper;

    private static final String STUDY_STATE_STUD = "studyState.stud";


    @Test
    public void test() throws Exception {
        List<String> lines = mock(List.class);
        when(fileParser.parse(STUDY_STATE_STUD)).thenReturn(lines);
        StudyProgram expected = mock(StudyProgram.class);
        when(mapper.map(lines)).thenReturn(expected);

        StudyProgram actual = tested.retrieve();

        assertThat(actual, is(expected));
    }
}