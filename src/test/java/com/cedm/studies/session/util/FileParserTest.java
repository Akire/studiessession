package com.cedm.studies.session.util;

import com.cedm.studies.session.exception.FileParseException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class FileParserTest {

    @InjectMocks
    FileParser tested;

    @Test
    public void testParse() throws Exception {
        List<String> expected = Arrays.asList("Obama name ?| Barack", "France capitale ?| Paris");

        List<String> actual = tested.parse("target/test-classes/TestFile.txt");

        assertThat(actual, is(expected));
    }

    @Test(expected = FileParseException.class)
    public void testUnknownFile() throws Exception {
        tested.parse("unknow.txt");
    }
}