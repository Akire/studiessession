package com.cedm.studies.session.model;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;


public class StudyProgramTest {

    @Test
    public void testAddRedCard() throws Exception {
        StudyProgram studyProgram = new StudyProgram();
        Card card = mock(Card.class);

        boolean status = studyProgram.putInStudyProgram("r", card);

        assertTrue(status);
    }

    @Test
    public void testAddOrangeCard() throws Exception {
        StudyProgram studyProgram = new StudyProgram();
        Card card = mock(Card.class);

        boolean status = studyProgram.putInStudyProgram("o", card);

        assertTrue(status);
    }

    @Test
    public void testAddGreenCard() throws Exception {
        StudyProgram studyProgram = new StudyProgram();
        Card card = mock(Card.class);

        boolean status = studyProgram.putInStudyProgram("g", card);

        assertTrue(status);
    }

    @Test
    public void testAddRandomCard() throws Exception {
        StudyProgram studyProgram = new StudyProgram();
        Card card = mock(Card.class);

        boolean status = studyProgram.putInStudyProgram("t", card);

        assertFalse(status);
    }

    @Test
    public void testReadyForExamWithOnlyGreenCard() throws Exception {
        StudyProgram studyProgram = new StudyProgram();
        Card card = mock(Card.class);
        studyProgram.putInStudyProgram("g", card);

        assertTrue(studyProgram.readyForExam());
    }

    @Test
         public void testNotReadyForExamWithAtLeastOneOrange() throws Exception {
        StudyProgram studyProgram = new StudyProgram();
        Card cardOrange = mock(Card.class);
        Card cardGreen = mock(Card.class);
        studyProgram.putInStudyProgram("o", cardOrange);
        studyProgram.putInStudyProgram("g", cardGreen);

        assertFalse(studyProgram.readyForExam());
    }

    @Test
    public void testNotReadyForExamWithAtLestOneRed() throws Exception {
        StudyProgram studyProgram = new StudyProgram();
        Card cardRed = mock(Card.class);
        Card cardGreen = mock(Card.class);
        studyProgram.putInStudyProgram("r", cardRed);
        studyProgram.putInStudyProgram("g", cardGreen);

        assertFalse(studyProgram.readyForExam());
    }

    @Test
    public void testStillWorkForToday() throws Exception {
        StudyProgram studyProgram = new StudyProgram();
        Card card = mock(Card.class);
        studyProgram.putInStudyProgram("r", card);

        assertTrue(studyProgram.stillWorkForToday());
    }

    @Test
    public void testNotWorkForToday() throws Exception {
        StudyProgram studyProgram = new StudyProgram();
        Card card = mock(Card.class);
        studyProgram.putInStudyProgram("o", card);

        assertFalse(studyProgram.stillWorkForToday());
    }

    @Test
    public void testGetDailyWork() throws Exception {
        StudyProgram studyProgram = new StudyProgram();
        Card cardFirst = mock(Card.class);
        Card cardSecond = mock(Card.class);
        studyProgram.putInStudyProgram("r", cardFirst);
        studyProgram.putInStudyProgram("r", cardSecond);

        Card actualFirstWork = studyProgram.getDailyWork();
        Card actualSecondWork = studyProgram.getDailyWork();

        assertThat(actualFirstWork, is(cardFirst));
        assertThat(actualSecondWork, is(cardSecond));
    }
}