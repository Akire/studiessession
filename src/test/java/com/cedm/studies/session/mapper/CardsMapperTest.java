package com.cedm.studies.session.mapper;

import com.cedm.studies.session.model.Card;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CardsMapperTest {

    CardsMapper tested;

    @Before
    public void setUp() throws Exception {
        tested = new CardsMapper();
    }

    @Test
    public void testMap() throws Exception {
        List<String> lines = Arrays.asList("Obama name ?| Barack");

        List<Card> actual = tested.map(lines);

        Card barack = new Card("Obama name ?", "Barack");
        assertThat(actual, is(Arrays.asList(barack)));
    }

    @Test
    public void testSortAlphabetic() throws Exception {
        List<String> lines = Arrays.asList("Obama name ?| Barack", "La capitale de France ?| Paris");

        List<Card> actual = tested.map(lines);

        Card paris = new Card("La capitale de France ?", "Paris");
        Card barack = new Card("Obama name ?", "Barack");
        assertThat(actual, is(Arrays.asList(paris, barack)));
    }

    @Test
    public void testMapWithoutLines() throws Exception{
        List<Card> actual = tested.map(new ArrayList<>());

        assertThat(actual, is(new ArrayList<>()));
    }


    @Test(expected = IllegalArgumentException.class)
    public void testMapWithBadFormat() throws Exception {
        tested.map(Arrays.asList("Obama name ?"));
    }
}