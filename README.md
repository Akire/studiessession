########## Studies session ##########

Ce programme est écrit en java 8 et avec framework spring boot. Le build se fait via maven.

###### Lancer l'application ######

Pour une nouvelle session il faut lancer cette commande : mvn spring-boot:run -Drun.arguments="<fileName>"
Pour continuer une session il faut executer cette commande : mvn spring-boot:run

Après la fin d'une session, le fichier studyState.stub contenant les informations sur la prochaine session
